let n;
function fib(n) {
  let a = 1,
    b = 1,
    x = -1,
    y = -1;
  if (n < 0) {
    for (let j = -3; j >= n; j--) {
      let z = x + y;
      x = -y;
      y = -z;
    } return y;
  } else {
    for (let i = 3; i <= n; i++) {
      let c = a + b;
      a = b;
      b = c;
    }
    return b;
  }
}
n = +prompt('Enter the number')
alert(n + ' Numer of Fibonacci row' + ' is ' + fib(n));